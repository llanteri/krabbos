use std::fs;

use clap::Parser;

#[derive(Parser, Debug)]
struct Args {
    /// Use UEFI instead of BIOS
    #[arg(long)]
    uefi: bool,

    /// Make QEMU wait on a GDB connection on TCP port 1234
    ///
    /// GDB should be launched as `gdb -x krabbos.gdb`
    #[arg(long)]
    gdb: bool,

    /// Use the standard output for the QEMU monitor
    ///
    /// The debug console is redirected to `log.txt`
    #[arg(long)]
    monitor: bool,

    /// Additional arguments for QEMU
    #[arg(last = true)]
    qemu_args: Vec<String>,
}

fn main() {
    // read env variables that were set in build script
    let kernel_path = env!("KERNEL_PATH");
    let uefi_path = env!("UEFI_PATH");
    let bios_path = env!("BIOS_PATH");

    let args = Args::parse();

    // run QEMU
    let mut cmd = std::process::Command::new("qemu-system-x86_64");
    cmd.args(&args.qemu_args);
    if args.monitor {
        cmd.args(["-debugcon", "file:log.txt"]);
        cmd.args(["-monitor", "stdio"]);
    } else {
        cmd.args(["-debugcon", "stdio"]);
    }
    if args.uefi {
        cmd.arg("-bios").arg(ovmf_prebuilt::ovmf_pure_efi());
        cmd.arg("-drive")
            .arg(format!("format=raw,file={uefi_path}"));
    } else {
        cmd.arg("-drive")
            .arg(format!("format=raw,file={bios_path}"));
    }
    if args.gdb {
        cmd.args(["-s", "-S"]);

        fs::write(
            "krabbos-kernel.gdb",
            format!("add-symbol-file {kernel_path} -o 0x8000000000\n"),
        )
        .expect("failed to write krabbos-kernel.gdb");
    }
    let mut child = cmd.spawn().unwrap();
    child.wait().unwrap();
}
