use std::path::Path;
use std::path::PathBuf;

fn main() {
    // set by cargo
    let out_dir = PathBuf::from(std::env::var_os("OUT_DIR").unwrap());
    let kernel =
        PathBuf::from(std::env::var_os("CARGO_BIN_FILE_KRABBOS_KERNEL_krabbos_kernel").unwrap());

    let uefi_path = out_dir.join("uefi.img");
    let bios_path = out_dir.join("bios.img");
    bootloader::UefiBoot::new(&kernel)
        .set_ramdisk(Path::new("filesystems/disk.img"))
        .create_disk_image(&uefi_path)
        .unwrap();
    bootloader::BiosBoot::new(&kernel)
        .set_ramdisk(Path::new("filesystems/disk.img"))
        .create_disk_image(&bios_path)
        .unwrap();

    // pass the disk image paths as env variables to the `main.rs`
    println!("cargo:rustc-env=KERNEL_PATH={}", kernel.display());
    println!("cargo:rustc-env=UEFI_PATH={}", uefi_path.display());
    println!("cargo:rustc-env=BIOS_PATH={}", bios_path.display());
}
