use alloc::vec::Vec;
use bootloader_api::info::{FrameBuffer, PixelFormat};
use core::{
    fmt::{self, Error},
    ptr,
};
use noto_sans_mono_bitmap::{get_raster, FontWeight, RasterHeight, RasterizedChar};
use spin::Mutex;
use x86_64::instructions::interrupts::without_interrupts;

use embedded_graphics::{pixelcolor::Rgb888, prelude::*};
use tinybmp::Bmp;

const BORDER: usize = 1;
const LETTER_SPACING: usize = 0;
const LINE_SPACING: usize = 2;
const SIZE: RasterHeight = RasterHeight::Size20;
const UNKNOWN_CHAR: char = '�';
#[allow(dead_code)] // Very important piece of code. DO NOT DELETE
const NO_DELETE_BACKSLASH: bool = false;
const CURSOR_COLOR: Color = Color::white();

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Position {
    pub x: usize,
    pub y: usize,
}

impl core::ops::Add<(usize, usize)> for Position {
    type Output = Self;
    fn add(self, rhs: (usize, usize)) -> Self::Output {
        Self {
            x: self.x + rhs.0,
            y: self.y + rhs.1,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

impl Color {
    /*pub fn from_slice(slice: [u8; 3]) -> Self {
        Color {
            red: slice[0],
            green: slice[1],
            blue: slice[2],
        }
    }*/

    pub fn as_slice(&self, pixel_format: PixelFormat) -> [u8; 4] {
        match pixel_format {
            PixelFormat::Rgb => [self.red, self.green, self.blue, 0],
            PixelFormat::Bgr => [self.blue, self.green, self.red, 0],
            PixelFormat::U8 => {
                // take the average color
                let gray = self.red / 3 + self.green / 3 + self.blue / 3;
                [if gray > 200 { 0xf } else { 0 }, 0, 0, 0]
            }
            other => panic!("unknown pixel format {other:?}"),
        }
    }

    pub const fn mul_byte(&self, byte: u8) -> Color {
        Color {
            red: (self.red as u16 * byte as u16 / u8::MAX as u16) as u8,
            green: (self.green as u16 * byte as u16 / u8::MAX as u16) as u8,
            blue: (self.blue as u16 * byte as u16 / u8::MAX as u16) as u8,
        }
    }

    #[allow(dead_code)]
    pub const fn white() -> Color {
        Color {
            red: 255,
            green: 255,
            blue: 255,
        }
    }

    #[allow(dead_code)]
    pub const fn black() -> Color {
        Color {
            red: 0,
            green: 0,
            blue: 0,
        }
    }

    #[allow(dead_code)]
    pub const fn red() -> Color {
        Color {
            red: 255,
            green: 0,
            blue: 0,
        }
    }

    #[allow(dead_code)]
    pub const fn blue() -> Color {
        Color {
            red: 0,
            green: 0,
            blue: 255,
        }
    }

    #[allow(dead_code)]
    pub const fn green() -> Color {
        Color {
            red: 0,
            green: 255,
            blue: 0,
        }
    }

    #[allow(dead_code)]
    pub const fn yellow() -> Color {
        Color {
            red: 255,
            green: 229,
            blue: 0,
        }
    }

    #[allow(dead_code)]
    pub const fn purple() -> Color {
        Color {
            red: 125,
            green: 29,
            blue: 211,
        }
    }

    #[allow(dead_code)]
    pub const fn orange() -> Color {
        Color {
            red: 255,
            green: 165,
            blue: 0,
        }
    }

    #[allow(dead_code)]
    pub const fn light_blue() -> Color {
        Color {
            red: 0,
            green: 255,
            blue: 246,
        }
    }
}

/// A VGA writer to display text to the screen
struct Writer {
    /// The framebuffer, i.e. the pixels of the screen
    framebuffer: Option<&'static mut FrameBuffer>,
    /// The current position of the text cursor
    position: Position,
    /// This contains a buffer for the current line, since we cannot retrieve it quickly from the pixels.
    line_buffer: (Vec<char>, usize),
    /// Current text color.
    color: Color,
    /// Current font weight.
    weight: FontWeight,
    display_cursor: bool,
}

impl Writer {
    /// Creates a new writer with default color white.
    /// Warning: the writer does not clear the buffer.
    pub const fn new() -> Self {
        Writer {
            framebuffer: None,
            position: Position {
                x: BORDER,
                y: BORDER,
            },
            line_buffer: (Vec::new(), 0),
            color: Color::white(),
            weight: FontWeight::Regular,
            display_cursor: false,
        }
    }

    /// Change the color used to display text.
    pub fn set_color(&mut self, color: Color) {
        self.color = color
    }

    /// Change the weight of the font.
    pub fn set_weight(&mut self, weight: FontWeight) {
        self.weight = weight
    }

    pub fn set_cursor(&mut self, display_cursor: bool) {
        self.display_cursor = display_cursor
    }

    /// Retrieves the raster of a character.
    fn rasterizer(&self, c: char) -> RasterizedChar {
        get_raster(c, self.weight, SIZE).unwrap_or_else(|| {
            get_raster(UNKNOWN_CHAR, self.weight, SIZE)
                .expect("Could not get raster of backup character.")
        })
    }

    fn set_framebuffer(&mut self, framebuffer: &'static mut FrameBuffer) {
        self.framebuffer = Some(framebuffer);
    }

    fn newline(&mut self) {
        self.write_cursor(false);
        let tmp = &self.line_buffer.0[self.line_buffer.1..];
        self.line_buffer = (tmp.to_vec(), 0);
        for i in 0..self.line_buffer.0.len() {
            let rasterized_char = self.rasterizer(self.line_buffer.0[i]);
            self.write_black(&rasterized_char);
            self.position.x += rasterized_char.width();
        }
        self.position.y += SIZE.val() + LINE_SPACING;
        self.position.x = BORDER;
        for i in 0..self.line_buffer.0.len() {
            let rasterized_char = self.rasterizer(self.line_buffer.0[i]);
            self.write_rasterized_char(&rasterized_char);
        }
        self.line_buffer.1 = 0;
        self.position.x = BORDER;
        self.write_cursor(true);
    }

    /// Clears the frame buffer and resets the position
    pub fn clear(&mut self) {
        self.write_cursor(false);
        self.position.x = BORDER;
        self.position.y = BORDER;
        self.get_framebuffer().buffer_mut().fill(0);
        self.line_buffer = (Vec::new(), 0);
        self.write_cursor(true);
    }

    /// Takes a rasterized char and writes in into the framebuffer.
    /// Also updates the position.
    fn write_rasterized_char(&mut self, rasterized_char: &RasterizedChar) {
        for (y, row) in rasterized_char.raster().iter().enumerate() {
            for (x, byte) in row.iter().enumerate() {
                self.write_pixel(self.position + (x, y), self.color.mul_byte(*byte));
            }
        }
        self.position = self.position + (rasterized_char.width() + LETTER_SPACING, 0);
    }

    /// Returns a reference to the framebuffer and displays an error
    /// if it was not initialised.
    fn get_framebuffer(&mut self) -> &mut &'static mut FrameBuffer {
        self.framebuffer
            .as_mut()
            .expect("framebuffer = None ; this should have been caught by 'write_str'")
    }

    /// Writes the given byte at the given pixel position.
    fn write_pixel(&mut self, pixel_pos: Position, color: Color) {
        let bpp = self.get_framebuffer().info().bytes_per_pixel;
        if (pixel_pos.x + pixel_pos.y * self.get_framebuffer().info().width) * bpp
            >= self.get_framebuffer().info().byte_len
        {
            return;
        }
        let pixel_format = self.get_framebuffer().info().pixel_format;
        let pixel = pixel_pos.y * self.get_framebuffer().info().stride + pixel_pos.x;

        self.get_framebuffer().buffer_mut()[pixel * bpp..(pixel * bpp + bpp)]
            .copy_from_slice(&color.as_slice(pixel_format)[..bpp]);

        // SAFETY: a reference is guaranteed to be valid, aligned and initialized
        unsafe {
            ptr::read_volatile(&self.get_framebuffer().buffer_mut()[pixel * bpp]);
        };
    }

    fn write_black(&mut self, rasterized_char: &RasterizedChar) {
        for y in 0..rasterized_char.height() {
            for x in 0..rasterized_char.width() {
                self.write_pixel(self.position + (x, y), Color::black());
            }
        }
    }

    fn write_cursor(&mut self, visible: bool) {
        if !self.display_cursor {
            return;
        }
        let cursor_color = if visible {
            CURSOR_COLOR
        } else {
            Color::black()
        };

        // Antoine said this works
        // Well you gotta take care about not writing outside the canvas tho...
        let rasterized_char = self.rasterizer('\\');
        for y in 0..rasterized_char.height() {
            self.write_pixel(self.position + (0, y), cursor_color);
            self.write_pixel(self.position + (rasterized_char.width(), y), cursor_color);
        }
        for x in 0..rasterized_char.width() {
            self.write_pixel(self.position + (x, 0), cursor_color);
            self.write_pixel(self.position + (x, rasterized_char.height()), cursor_color);
        }
    }

    /// If not on the left side of the screen, delete the last character added to the column.
    pub fn backspace(&mut self) {
        if self.line_buffer.1 == 0 {
            return;
        }
        self.write_cursor(false);
        let rasterized_char = self.rasterizer(self.line_buffer.0[self.line_buffer.1 - 1]);
        self.position.x -= rasterized_char.width();
        let beta = self.position.x;
        self.line_buffer.1 -= 1;
        self.line_buffer.0.remove(self.line_buffer.1);

        for i in self.line_buffer.1..self.line_buffer.0.len() {
            let _rasterized_char = self.rasterizer(self.line_buffer.0[i]);
            self.write_rasterized_char(&_rasterized_char);
        }
        self.write_black(&rasterized_char);
        self.position.x = beta;
        self.write_cursor(true);
    }

    pub fn move_cursor(&mut self, d: i8) {
        match d {
            1 if self.line_buffer.1 < self.line_buffer.0.len() => {
                self.write_cursor(false);
                let rasterized_char = self.rasterizer(self.line_buffer.0[self.line_buffer.1]);
                self.position.x += rasterized_char.width();
                self.line_buffer.1 += 1;
                self.write_cursor(true);
            }
            -1 if self.line_buffer.1 > 0 => {
                self.write_cursor(false);
                let rasterized_char = self.rasterizer(self.line_buffer.0[self.line_buffer.1 - 1]);
                self.position.x -= rasterized_char.width();
                self.line_buffer.1 -= 1;
                self.write_cursor(true);
            }
            _ => (),
        }
    }

    /// Writes a character to the frame buffer.
    pub fn write_char(&mut self, c: char) {
        if self.line_buffer.1 == self.line_buffer.0.len() {
            match c {
                '\r' => {
                    self.position.x = BORDER;
                    self.line_buffer.1 = 0;
                }
                '\n' => {
                    // This push is useless but it might prove useful if we want paragraphs
                    // self.line_buffer.0.push(c);
                    self.newline();
                }
                _ => {
                    self.write_cursor(false);
                    self.line_buffer.0.push(c);
                    self.line_buffer.1 += 1;
                    // If the end of the line is reached (the character would be written outside)
                    if self.position.x >= self.get_framebuffer().info().width {
                        self.newline();
                    }
                    // If the bottom of screen is reached
                    if self.position.y + SIZE.val() + BORDER >= self.get_framebuffer().info().height
                    {
                        // TODO: screen scolling
                        self.clear();
                    }

                    let rasterized_char = self.rasterizer(c);
                    self.write_rasterized_char(&rasterized_char);
                    self.write_cursor(true);
                }
            }
        } else {
            match c {
                '\r' => {
                    self.position.x = BORDER;
                    self.line_buffer.1 = 0;
                }
                '\n' => {
                    // Same goes with this push
                    // self.line_buffer.0.insert(self.line_buffer.1 - 1, c);
                    self.newline();
                }
                _ => {
                    self.write_cursor(false);
                    self.line_buffer.1 += 1;
                    // If the end of the line is reached (the character would be written outside)
                    if self.position.x >= self.get_framebuffer().info().width {
                        self.newline();
                    }
                    // If the bottom of screen is reached
                    if self.position.y + SIZE.val() + BORDER >= self.get_framebuffer().info().height
                    {
                        // TODO: screen scolling
                        self.clear();
                    }

                    let rasterized_char = self.rasterizer(c);
                    self.write_rasterized_char(&rasterized_char);
                    self.write_cursor(true);
                }
            }
        }
    }

    pub fn delete(&mut self) {
        if self.line_buffer.1 >= self.line_buffer.0.len() {
            return;
        }
        self.write_cursor(false);
        let rasterized_char = self.rasterizer(self.line_buffer.0[self.line_buffer.1]);
        let beta = self.position.x;
        self.line_buffer.0.remove(self.line_buffer.1);

        for i in self.line_buffer.1..self.line_buffer.0.len() {
            let rasterized_char = self.rasterizer(self.line_buffer.0[i]);
            self.write_rasterized_char(&rasterized_char);
        }
        self.write_black(&rasterized_char);
        self.position.x = beta;
        self.write_cursor(true);
    }

    pub fn write_string(&mut self, s: &str) {
        for c in s.chars() {
            self.write_char(c);
        }
    }

    pub fn readu_bitumappu(&mut self, bmp: &[u8]) -> Result<(), tinybmp::ParseError> {
        // Il y a eu des tentatives d'essai.
        match Bmp::<Rgb888>::from_slice(bmp) {
            Ok(bmp) => {
                let (width, height) = (bmp.size().width as usize, bmp.size().height as usize);

                let centered_position = (
                    (self.framebuffer.as_ref().unwrap().info().width - width) / 2,
                    (self.framebuffer.as_ref().unwrap().info().height - height) / 2,
                );

                for Pixel(position, color) in bmp.pixels() {
                    let position = Position {
                        x: position.x as usize,
                        y: position.y as usize,
                    };
                    let color = Color {
                        red: color.r(),
                        green: color.g(),
                        blue: color.b(),
                    };
                    self.write_pixel(position + centered_position, color)
                }
                Ok(())
            }
            Err(e) => Err(e),
        }
    }
}

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        match self.framebuffer {
            None => {
                debug_println!(
                    "cannot write string {:?} on VGA output: VGA was not initialised",
                    s
                );
                Err(Error)
            }
            Some(_) => {
                self.write_string(s);
                Ok(())
            }
        }
    }
}

static WRITER: Mutex<Writer> = Mutex::new(Writer::new());

/// Initialises the VGA buffer. Call to macros such as `print!` can only
/// occur after this function is called. The second argument is the color
/// used to display text; if it is set to `None`, `Color::white` will be used.
pub fn init_vga(framebuffer: &'static mut FrameBuffer, color: Option<Color>) {
    debug_println!("Initializing VGA buffer...");
    let (w, h) = (
        framebuffer.info().width / (9 + LETTER_SPACING),
        framebuffer.info().height / (20 + LINE_SPACING),
    );
    without_interrupts(|| {
        let mut writer = WRITER.lock();
        writer.set_color(color.unwrap_or(Color::white()));
        writer.set_framebuffer(framebuffer);
        writer.clear();
    });
    debug_println!("VGA buffer initialised with canvas of size {}x{}", w, h);
}

pub fn backspace() {
    without_interrupts(|| {
        let mut writer = WRITER.lock();
        writer.backspace()
    });
}

pub fn delete() {
    without_interrupts(|| {
        let mut writer = WRITER.lock();
        writer.delete();
    });
}

pub fn clear() {
    without_interrupts(|| {
        let mut writer = WRITER.lock();
        writer.clear();
    });
}

/// The values that are passed to move_cursor in the functions below are arbitrary
/// They are used not to have 3 gazillion functions in the struct impl for WRITER.
/// The value is NOT a position on the line.
/// TODO : Implement Up and Down, if possible without a full buffer ? How to do so ?
pub fn we_go_left() {
    WRITER.lock().move_cursor(-1)
}

pub fn we_go_right() {
    WRITER.lock().move_cursor(1)
}

pub fn _we_go_down() {
    WRITER.lock().move_cursor(-2)
}

pub fn _we_go_up() {
    WRITER.lock().move_cursor(2)
}

pub fn set_weight(weight: FontWeight) {
    without_interrupts(|| {
        let mut writer = WRITER.lock();
        writer.set_weight(weight);
    });
}

pub fn set_color(color: Color) {
    without_interrupts(|| {
        let mut writer = WRITER.lock();
        writer.set_color(color);
    });
}

pub fn set_cursor(display_cursor: bool) {
    without_interrupts(|| {
        let mut writer = WRITER.lock();
        writer.set_cursor(display_cursor);
    });
}

#[macro_export] // available to the whole crate
macro_rules! println  {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga_buffer::_print(format_args!($($arg)*)));
}

#[doc(hidden)] // hide it from the generated documentation
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    use x86_64::instructions::interrupts;

    interrupts::without_interrupts(|| {
        let _ = WRITER.lock().write_fmt(args);
    });
}

pub fn display_bmp(bmp: &[u8]) {
    let _ = without_interrupts(|| match WRITER.lock().readu_bitumappu(bmp) {
        Ok(_) => (),
        Err(e) => println!("imshow: {:?}", e),
    });
}
