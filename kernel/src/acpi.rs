//! Fetching and parsing [Advanced Configuration and Power Interface (ACPI)][acpi] tables
//!
//! [acpi]: https://wiki.osdev.org/Acpi

use core::{
    mem::{self, offset_of, size_of},
    slice,
};

use crate::paging::phys_as_ptr;

/// The [Root System Description Pointer][rsdp], entry point to the ACPI tables
///
/// For ACPI 2.0 (i.e. for `revision` = 2), this needs to be extended into
/// the  XSDP (TODO).
///
/// [rsdp]: https://wiki.osdev.org/RSDP
#[repr(C, packed)]
#[derive(Debug, Clone, Copy)]
struct Rsdp {
    signature: [u8; 8],
    checksum: u8,
    oemid: [u8; 6],
    revision: u8,
    rsdt_address: u32,
}

/// A System Description Table header
#[repr(C, packed)]
#[derive(Debug, Clone, Copy)]
pub struct SdtHeader {
    pub signature: [u8; 4],
    pub length: u32,
    revision: u8,
    checksum: u8,
    oem_id: [u8; 6],
    oem_table_id: [u8; 8],
    oem_revision: u32,
    creator_id: u32,
    creator_revision: u32,
}

/// The [Root System Description Table][rsdt], the table containing pointers to
/// all the others SDTs.
///
/// [rsdt]: https://wiki.osdev.org/RSDT
#[repr(transparent)]
#[derive(Debug, Clone, Copy)]
pub struct Rsdt(SdtHeader);

/// The [Multiple APIC Description Table][madt] (signature `"APIC"`)
///
/// Contains a lot of information related to APICs on the system, in the form of
/// [`MadtRecord`]s. In particular:
/// - all processors and their associated Local APIC IDs, required for
///   routing IRQs to given processors
/// - all I/O APICs with their base address and the IRQs they handle, required
///   for configuring interrupt routing
/// - a list of interrupt source overrides, to be taken into consideration when
///   hooking up IRQs
///
/// [madt]: https://wiki.osdev.org/MADT
#[repr(C, packed)]
#[derive(Debug)]
pub struct Madt {
    header: SdtHeader,
    lapic_addr: u32,
    flags: u32,
    records: [u8; 0],
}

#[repr(C, packed)]
#[derive(Debug)]
pub struct ProcessorLocalApic {
    pub processor_id: u8,
    pub apic_id: u8,
    pub flags: u32,
}

#[repr(C, packed)]
#[derive(Debug)]
pub struct InputOutputApic {
    pub id: u8,
    #[doc(hidden)]
    __rsv: u8,
    pub addr: u32,
    pub global_system_interrupt_base: u32,
}

#[repr(C, packed)]
#[derive(Debug)]
pub struct InterruptSourceOverride {
    pub bus_source: u8,
    pub irq_source: u8,
    pub global_system_interrupt: u32,
    pub flags: u16,
}

#[repr(C, packed)]
#[derive(Debug)]
pub struct NonMaskableInterrupts {
    pub processor_id: u8,
    pub flags: u16,
    pub lint_no: u8,
}

#[derive(Debug)]
pub enum MadtRecord {
    ProcessorLocalAPIC(&'static ProcessorLocalApic),
    InputOutputAPIC(&'static InputOutputApic),
    InterruptSourceOverride(&'static InterruptSourceOverride),
    NonMaskableInterrupts(&'static NonMaskableInterrupts),
    Unknown,
}

struct RecordsIter {
    madt: &'static Madt,
    offset: isize,
}

impl Iterator for RecordsIter {
    type Item = MadtRecord;
    fn next(&mut self) -> Option<Self::Item> {
        if self.offset >= self.madt.header.length as isize {
            return None;
        }
        // SAFETY: offset <= self.length is still in bounds
        let entry_ptr = unsafe { (self.madt as *const _ as *const u8).offset(self.offset) };
        // SAFETY: same reason as above
        let (entry_type, length) = unsafe { (*entry_ptr, *entry_ptr.offset(1)) };
        self.offset += length as isize;
        // SAFETY: `entry_type` caracterizes the length and layout of data
        unsafe {
            match entry_type {
                0 => Some(MadtRecord::ProcessorLocalAPIC(
                    &*(entry_ptr.offset(2) as *const ProcessorLocalApic),
                )),
                1 => Some(MadtRecord::InputOutputAPIC(
                    &*(entry_ptr.offset(2) as *const InputOutputApic),
                )),
                2 => Some(MadtRecord::InterruptSourceOverride(
                    &*(entry_ptr.offset(2) as *const InterruptSourceOverride),
                )),
                4 => Some(MadtRecord::NonMaskableInterrupts(
                    &*(entry_ptr.offset(2) as *const NonMaskableInterrupts),
                )),
                _ => Some(MadtRecord::Unknown),
            }
        }
    }
}

impl Madt {
    pub fn records(&'static self) -> impl Iterator<Item = MadtRecord> {
        RecordsIter {
            madt: self,
            offset: offset_of!(Self, records) as isize,
        }
    }
}

pub unsafe fn sdt_iter(rsdp_addr: u64) -> impl Iterator<Item = &'static SdtHeader> + Clone {
    // SAFETY: caller must ensure this is the real, real, real RSDP
    let rsdp: &Rsdp = unsafe { &*(phys_as_ptr(rsdp_addr)) };
    assert!(rsdp.revision == 0, "ACPI version >= 2.0");
    assert!(rsdp.validate(), "RSDP validation failed");

    // SAFETY: RSDP has been validated, now there's only a 1/256 chance things explode
    let rsdt: &Rsdt = unsafe { &*(phys_as_ptr(rsdp.rsdt_address as u64)) };
    assert!(rsdt.0.checksum(), "RSDT validation failed");

    rsdt.sdts()
}

impl Rsdp {
    fn validate(&self) -> bool {
        self.signature == *b"RSD PTR " && self.checksum()
    }

    fn checksum(&self) -> bool {
        // SAFETY: we stay in the bounds of `self` (and `u8` is always properly aligned)
        let bytes =
            unsafe { slice::from_raw_parts(self as *const _ as *const u8, mem::size_of_val(self)) };
        bytes.iter().copied().fold(0, u8::wrapping_add) == 0
    }
}

impl SdtHeader {
    fn checksum(&self) -> bool {
        // SAFETY: we stay in the bounds of `self` as given by `self.length`
        // (and `u8` is always properly aligned)
        let bytes =
            unsafe { slice::from_raw_parts(self as *const _ as *const u8, self.length as usize) };
        bytes.iter().copied().fold(0, u8::wrapping_add) == 0
    }

    fn data(&self) -> &[u8] {
        // SAFETY: we stay in the bounds of `self` as given by `self.length`
        unsafe {
            slice::from_raw_parts(
                (self as *const _ as *const u8).byte_add(size_of::<Self>()),
                self.length as usize - size_of::<Self>(),
            )
        }
    }
}

impl Rsdt {
    fn sdts(&'static self) -> impl Iterator<Item = &'static SdtHeader> + Clone {
        // The RSDT isn't generally properly aligned so we have to cast to 1-aligned `u8`s
        // SAFETY: we stay in the bounds of `self` as given by `self.header.length`.
        // It's not like the substraction of length - size of header could overflow, right?
        self.0.data().chunks(4).map(move |addr| {
            let addr = u32::from_le_bytes(addr.try_into().unwrap());
            // SAFETY: we followed a pointer from the RSDT
            // FIXME: still probaby UB because of alignment
            unsafe {
                let header: &SdtHeader = &*phys_as_ptr(addr as u64);
                assert!(
                    header.checksum(),
                    "SDT header validation failed: {header:#?}"
                );
                header
            }
        })
    }
}
