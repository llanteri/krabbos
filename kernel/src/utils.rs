use core::cell::UnsafeCell;

/// A lazily-initialized cell, where the initializer is tasked to ensure
/// initialization happens before any reads.
pub struct OnceUnsafe<T>(UnsafeCell<Option<T>>);

impl<T> OnceUnsafe<T> {
    pub const fn new() -> OnceUnsafe<T> {
        OnceUnsafe(UnsafeCell::new(None))
    }

    /// Initialize the contents of the cell
    ///
    /// # Safety
    ///
    /// The caller must ensure to call this before *any* access to the cell.
    pub unsafe fn init(&self, x: T) {
        // SAFETY: the caller must guarantee the access is unique
        unsafe {
            debug_assert!((*self.0.get()).is_none());
            *self.0.get() = Some(x);
        }
    }

    /// Access the contents of the cell
    pub fn get(&self) -> &T {
        // SAFETY: the only mutations of `self.0` are through `self.init()` whose
        // caller must guarantee it is executed before any accesses
        unsafe { &*self.0.get() }
            .as_ref()
            .expect("access to `OnceUnsafe` before initialization")
    }
}

// SAFETY: it's up to the caller of `init` to ensure the cell is never read
// before being written to
unsafe impl<T: Send + Sync> Sync for OnceUnsafe<T> {}
