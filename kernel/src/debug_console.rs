//! Writing to QEMU's `debugcon` device

use core::fmt::{self, Write};

use spin::Mutex;
use x86_64::instructions::interrupts::without_interrupts;
use x86_64::instructions::port::Port;

struct DebugConsole {
    port: Port<u8>,
}

static CONSOLE: Mutex<DebugConsole> = Mutex::new(DebugConsole {
    port: Port::new(0xE9),
});

impl fmt::Write for DebugConsole {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for b in s.bytes() {
            // SAFETY: debugcon port
            unsafe {
                self.port.write(b);
            }
        }
        Ok(())
    }
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    without_interrupts(|| {
        let _ = CONSOLE.lock().write_fmt(args);
    })
}

#[macro_export]
macro_rules! debug_print {
    ($($arg:tt)*) => {
        $crate::debug_console::_print(::core::format_args!($($arg)*))
    };
}

#[macro_export]
macro_rules! debug_println {
    () => (debug_print!("\n"));
    ($($arg:tt)*) => ($crate::debug_print!("{}\n", format_args!($($arg)*)));
}
