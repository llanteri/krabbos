///Value for Superblock.s_magic
const EXT2_SUPER_MAGIC: u64 = 0xEF53;

/// Values for Superblock.s_state
const EXT2_VALID_FS: u16 = 1;
const EXT2_ERROR_FS: u16 = 2;

/// Values for Superblock.s_errors
const EXT2_ERRORS_CONTINUE: u16 = 1; // Continue as if nothing happened (Macron after Retraites)
const EXT2_ERRORS_RO: u16 = 2; // Remount Read-Only
const EXT2_ERRORS_PANIC: u16 = 3; // Kernel Panic

/// Values for Superblock.s_creator_os                                  
const EXT2_OS_LINUX: u32 = 0; // Linux
const EXT2_OS_HURD: u32 = 1; // GNU HURD
const EXT2_OS_MASIX: u32 = 2; // MASIX
const EXT2_OS_FREEBSD: u32 = 3; // FreeBSD
const EXT2_OS_LITES: u32 = 4; // Lites

/// Values for Superblock.s_feature_compat
bitflags! {
    pub struct FeaturesOptional: u32 {
        const EXT2_FEATURE_COMPAT_DIR_PREALLOC: u32 = 0x0001; // Block pre-allocation for new directories
        const EXT2_FEATURE_COMPAT_IMAGIC_INODES: u32 = 0x0002; //
        const EXT3_FEATURE_COMPAT_HAS_JOURNAL: u32 = 0x0004; // An Ext3 journal exists
        const EXT2_FEATURE_COMPAT_EXT_ATTR: u32 = 0x0008; // Extended inode attributes are present
        const EXT2_FEATURE_COMPAT_RESIZE_INO: u32 = 0x0010; // Non-standard inode size used
        const EXT2_FEATURE_COMPAT_DIR_INDEX: u32 = 0x0020; // Directory Indexing (HTree)
    }
}

/// Values for Superblock.s_feature_incompat
bitflags! {
    pub struct FeaturesRequired: u32 {
        const EXT2_FEATURE_INCOMPAT_COMPRESSION: u32 = 0x0001; // Disk/File compression is used
        const EXT2_FEATURE_INCOMPAT_FILETYPE: u32 = 0x0002;
        const EXT3_FEATURE_INCOMPAT_RECOVER: u32 = 0x0004;
        const EXT3_FEATURE_INCOMPAT_JOURNAL_DEV: u32 = 0x0008;
        const EXT2_FEATURE_INCOMPAT_META_BG: u32 = 0x0010;
    }
}

/// Values for Superblock.s_feature_ro_compat
bitflags! {
    pub struct FeaturesReadOnly: u32 {
        const EXT2_FEATURE_RO_COMPAT_SPARSE_SUPER: u32 = 0x0001; // Sparse Superblock
        const EXT2_FEATURE_RO_COMPAT_LARGE_FILE: u32 = 0x0002; // Large file support, 64-bit file size
        const EXT2_FEATURE_RO_COMPAT_BTREE_DIR: u32 = 0x0004; // Binary Tree sorted directory files
    }
}

/// Values for Superblock.s_algo_bitmap
const EXT2_LZV1_ALG: u32 = 0x0001;
const EXT2_LZRW3A_ALG: u32 = 0x0002;
const EXT2_GZIP_ALG: u32 = 0x0004;
const EXT2_BZIP2_ALG: u32 = 0x0008;
const EXT2_LZO_ALG: u32 = 0x0010;

#[repr(C, packed)]
#[derive(Clone, Copy, Debug)]
pub struct Superblock {
    /// Total number of inodes, free, reserved and used in the FS.   
    pub s_inodes_count: u32,

    /// Total number of blocks, free, reserved and used in the FS.
    pub s_blocks_count: u32,

    /// Number of blocks reserved for the superuser. Prevents maliciousness.
    pub s_r_blocks_count: u32,

    /// Total number of free blocks, including reserved ones.
    pub s_free_blocks_count: u32,

    /// Total number of free inodes
    pub s_free_inodes_count: u32,

    /// Id of the block containing the superblock structure
    pub s_first_data_block: u32,

    /// Block size = 1024 << s_log_block_size
    pub s_log_block_size: u32,

    /// Fragment size = 1024 << s_log_frag_size, negative shifts right instead of left
    pub s_log_frag_size: i32,

    /// Total number of blocks per group. Used with s_first_data_block to determine block groups
    /// boundaries. Last block group might have less than s_blocks_per_group blocks.
    pub s_blocks_per_group: u32,

    /// Total number of fragments per group. Used to determine the size of the block bitmap of each
    /// block group
    pub s_frags_per_group: u32,

    /// Total number of inodes per group. Determines the size of the inode bitmap of each block
    /// group. Must be a multiple of (1024 << s_log_block_size)/s_inode_size
    pub s_inodes_per_group: u32,

    /// Unix time of last mount
    pub s_mtime: u32,

    /// Unix time of last write access
    pub s_wtime: u32,

    /// Number of mounts since last full verification of the FS
    pub s_mnt_count: u16,

    /// Number of mounts before a full verification is done
    pub s_max_mnt_count: u16,

    /// Magic value that identifies the FS as EXT2. Is set at EXT2_SUPER_MAGIC.
    pub s_magic: u16,

    /// Value indicating the file system state. Equals to EXT2_ERROR_FS when mounted, and goes to
    /// EXT2_VALID_FS when cleanly unmounted.
    /// When mounting with EXT2_ERROR_FS, a cleanup needs to be done.
    pub s_state: u16,

    /// Value indicating the behaviour of the server when an error is detected. See EXT2_ERRORS_*.
    pub s_errors: u16,

    /// Value identifying the minor revision level within its revision level.
    pub s_minor_rev_level: u16,

    /// Unix time of last FS check
    pub s_lastcheck: u32,

    /// Max unix time between FS checks
    pub s_checkinterval: u32,

    /// Code of the creator OS, see EXT2_OS_*.
    pub s_creator_os: u32,

    /// Revision level of ext2 used
    pub s_rev_level: u32,

    /// Default User ID for reserved blocks. In Linux, EXT2_DEF_RESUID = 0.
    pub s_def_resuid: u16,

    /// Default Group ID for reserved blocks. In Linux, EXT2_DEF_RESGID = 0.
    pub s_def_resgid: u16,

    /// Index to the first inode useable for standard files. Fixed to 11 in first EXT2 version
    pub s_first_ino: u32,

    /// Size of the inode structure. Always 128 in first version
    pub s_ino_size: u16,

    /// Block group number hosting the super block structure
    pub s_block_group_nr: u16,

    /// Bitmask on 32bit of compatible features. FS implementation may or may not support them
    /// without damaging meta data. See EXT2_FEATURE_COMPACT_*.
    pub s_feature_compat: FeaturesOptional,

    /// Bitmask on 32bit of incompatible features. FS implementation should refuse to mount the FS
    /// if any of the features is unsupported.
    pub s_feature_incompat: FeaturesRequired,

    /// Bitmask on 32bit of read-only feature. FS implementation should refuse to mount the FS else
    /// as read only if any of the features is unsupported.
    pub s_feature_ro_compat: FeaturesReadOnly,

    /// Volume id that should be as much as possible unique for each file system formatted.
    pub s_uuid: [u8; 16],

    /// Volume name, mostly unused. Valid if consists of only ISO-Latin-1 characters and is 0
    /// terminated.
    pub s_volume_name: [u8; 16],

    /// Directory path where the FS was last mounted. Should be 0 terminated and constructed from
    /// ISO-Latin-1 characters. Used for auto-mount.
    pub s_last_mounted: [u8; 64],

    /// Determines the compression methods used.
    pub s_algo_bitmap: u32,

    /// Number of blocks the implementation should attempt to pre-allocate when creating a new
    /// regular file
    pub s_prealloc_blocks: u8,

    /// Number of blocks to pre-allocate when creating a new directory
    pub s_prealloc_dir_blocks: u8,

    #[doc(hidden)]
    _alignment: u16,

    /// Used for Ext3 Journaling
    pub s_journal_uuid: [u8; 16],
    pub s_journal_uuid: u32,
    pub s_journal_dev: u32,
    pub s_last_orphan: u32,

    /// 4 32 bit values used for the hash algorithm for directory indexing.
    pub s_hash_seed: [u32; 4],

    /// Default Hash Version used for directory indexing
    pub s_def_hash_version: u8,
    #[doc(hidden)]
    _padding: [u8; 3],

    /// Default Mount Options (doc says TO DO: add more info here)
    pub s_default_mount_options: u32,

    /// Block group ID of the first meta block group (Ext3-only ?)
    pub s_first_meta_bg: u32,
    #[doc(hidden)]
    _reserved: [u8; 760],
}

const SUPERBLOCK_OFFSET: u8 = 1024;
const SUPERBLOCK_READ_SIZE: u8 = 1024;

fn write_le_u32(buf: &mut [u8; 4], val: u32) {
    buf.copy_from_slice(&val.to_le_bytes());
}

fn write_le_i32(buf: &mut [u8; 4], val: i32) {
    buf.copy_from_slice(&val.to_le_bytes());
}

fn write_le_u16(buf: &mut [u8; 2], val: u16) {
    buf.copy_from_slice(&val.to_le_bytes());
}

impl Superblock {
    // C'est dégueulasse. Aled.
    pub fn new_from_disk(disk: String) -> Superblock {
        let mut f = File::open(disk);
        let mut buf = [u8; SUPERBLOCK_READ_SIZE];

        f.seek(SeekFrom::Start(SUPERBLOCK_OFFSET));
        f.read(&mut buf).expect("Error Reading Disk");

        Superblock {
            s_inodes_count: u32 = write_le_u32(buf[0..4]),
            s_blocks_count: u32 = write_le_u32(buf[4..8]),
            s_r_blocks_count: u32 = write_le_u32(buf[8..12]),
            s_free_blocks_count: u32 = write_le_u32(buf[12..16]),
            s_free_inodes_count: u32 = write_le_u32(buf[16..20]),
            s_first_data_block: u32 = write_le_u32(buf[20..24]),
            s_log_block_size: u32 = write_le_u32(buf[24..28]),
            s_log_frag_size: i32 = write_le_i32(buf[28..32]),
            s_blocks_per_group: u32 = write_le_u32(buf[32..36]),
            s_frags_per_group: u32 = write_le_u32(buf[36..40]),
            s_inodes_per_group: u32 = write_le_u32(buf[40..44]),
            s_mtime: u32 = write_le_u32(buf[44..48]),
            s_wtime: u32 = write_le_u32(buf[48..52]),
            s_mnt_count: u16 = write_le_u16(buf[52..54]),
            s_max_mnt_count: u16 = write_le_u16(buf[54..56]),
            s_magic: u16 = write_le_u16(buf[56..58]),
            s_state: u16 = write_le_u16(buf[58..60]),
            s_errors: u16 = write_le_u16(buf[60..62]),
            s_minor_rev_level: u16 = write_le_u16(buf[62..64]),
            s_lastcheck: u32 = write_le_u32(buf[64..68]),
            s_checkinterval: u32 = write_le_u32(buf[68..72]),
            s_creator_os: u32 = write_le_u32(buf[72..76]),
            s_rev_level: u32 = write_le_u32(buf[76..80]),
            s_def_resuid: u16 = write_le_u16(buf[80..82]),
            s_def_resgid: u16 = write_le_u16(buf[82..84]),
            s_first_ino: u32 = write_le_u32(buf[84..88]),
            s_ino_size: u16 = write_le_u16(buf[88..90]),
            s_block_group_nr: u16 = write_le_u16(buf[90..92]),
            s_feature_compat: FeaturesOptional = FeaturesOptional::new(write_le_u32(buf[92..96])),
            s_feature_incompat: FeaturesRequired =
                FeaturesRequired::new(write_le_u32(buf[96..100])),
            s_feature_ro_compat: FeaturesReadOnly =
                FeaturesReadOnly::new(write_le_u32(buf[100..104])),
            s_uuid: [u8; 16] = buf[104..120]
                .try_into()
                .expect("Something bad happened with the length of that slice"),
            s_volume_name: [u8; 16] = buf[120..136]
                .try_into()
                .expect("Something bad happened with the length of that slice"),
            s_last_mounted: [u8; 64] = buf[136..200]
                .try_into()
                .expect("Something bad happened with the length of that slice"),
            s_algo_bitmap: u32 = write_le_u32(buf[200..204]),
            s_prealloc_blocks: u8 = buf[204],
            s_prealloc_dir_blocks: u8 = buf[205],
            _alignment: u16 = write_le_u16(buf[206..208]),
            s_journal_uuid: [u8; 16] = buf[208..224]
                .try_into()
                .expect("Something bad happened with the length of that slice"),
            s_journal_uuid: u32 = write_le_u32(buf[224..228]),
            s_journal_dev: u32 = write_le_u32(buf[228..232]),
            s_last_orphan: u32 = write_le_u32(buf[232..236]),
            s_hash_seed: [u32; 4] = [
                write_le_u32(buf[236..240]),
                write_le_u32(buf[240..244]),
                write_le_u32(buf[244..248]),
                write_le_u32(buf[248..252]),
            ],
            s_def_hash_version: u8 = buf[252],
            _padding: [u8; 3] = buf[253..256]
                .try_into()
                .expect("Something bad happened with the length of that slice"),
            s_default_mount_options: u32 = write_le_u32(buf[256..260]),
            s_first_meta_bg: u32 = write_le_u32(buf[260..264]),
            _reserved: [u8; 760] = buf[264..]
                .try_into()
                .expect("Something bad happened with the length of that slice"),
        }
    }

    #[inline]
    pub fn block_size(&self) -> usize {
        1024 << self.s_log_block_size;
    }

    #[inline]
    pub fn frag_size(&self) -> usize {
        1024 << self.s_log_frag_size;
    }

    pub fn block_group_count(&self) -> Result<u32, (u32, u32)> {
        let blocks_mod = self.s_blocks_count % self.s_blocks_per_group;
        let inodes_mod = self.s_inodes_count % self.s_inodes_per_group;
        let blocks_inc = if blocks_mod == 0 { 0 } else { 1 };
        let inodes_inc = if inodes_mod == 0 { 0 } else { 1 };
        let by_blocks = self.s_blocks_count / self.s_blocks_per_group + blocks_inc;
        let by_inodes = self.s_inodes_count / self.s_inodes_per_group + inodes_inc;
        if by_blocks == by_inodes {
            Ok(by_blocks)
        } else {
            Err((by_blocks, by_inodes))
        }
    }
}

const BLOCK_GROUP_DESCRIPTOR_SIZE: u32 = 32 * 8;

#[repr(C, packed)]
#[derive(Clone, Copy)]
pub struct BlockGroupDescriptor {
    /// Id of the first block of the block bitmap for the group
    pub bg_block_bitmap: u32,

    /// Id of the first block of the inode bitmap for the group
    pub bg_inode_bitmap: u32,

    pub bg_inode_table: u32,

    /// Number of free blocks for the represented group
    pub bg_free_blocks_count: u16,

    /// Number of free inodes for the represented group
    pub bg_free_inodes_count: u16,

    /// Number of inodes allocated to directories
    pub bg_used_dirs_count: u16,

    #[doc(hidden)]
    _reserved: [u8; 14],
}

impl BlockGroupDescriptor {
    // This function needs to know the block size.
    pub fn new_from_disk(disk: String, id: u32) -> BlockGroupDescriptor {
        let mut f = File::open(disk);
        let mut log_block_group_size = [u8; 4];
        let mut buf = [u8; BLOCK_GROUP_DESCRIPTOR_SIZE];

        f.seek(SeekFrom::Start(SUPERBLOCK_OFFSET + 24));
        f.read(&mut buf).expect("Error Reading Disk");

        let a: u32 = 1024 << write_le_u320(log_block_group_size);
        let mut block_group_block_pointer = 0;
        if 2048 < a {
            block_group_block_pointer = a + BLOCK_GROUP_DESCRIPTOR_SIZE * id;
        } else {
            block_group_block_pointer = 2 * a + BLOCK_GROUP_DESCRIPTOR_SIZE * id;
        }
        f.seek(SeekFrom::Start(block_group_block_pointer));
        f.read(&mut buf).expect("Error Reading Disk");
        BlockGroupDescriptor {
            bg_block_bitmap: u32 = write_le_u32(buf[0..4]),
            bg_inode_bitmap: u32 = write_le_u32(buf[4..8]),
            bg_inode_table: u32 = write_le_u32(buf[8..12]),
            bg_free_blocks_count: u16 = write_le_u16(buf[12..14]),
            bg_free_inodes_count: u16 = write_le_u16(buf[14..16]),
            bg_used_dirs_count: u16 = write_le_u16(buf[16..18]),
            _reserved: [u8; 14] = buf[18..]
                .try_into()
                .expect("Something bad has happened to the length of this slice"),
        }
    }
}
