use bootloader_api::config::{Mapping, Mappings};
use bootloader_api::info::{MemoryRegionKind, MemoryRegions};
use x86_64::registers::control::Cr3;
use x86_64::structures::paging::OffsetPageTable;
use x86_64::structures::paging::{FrameAllocator, PhysFrame, Size4KiB};
use x86_64::PhysAddr;
use x86_64::{structures::paging::PageTable, VirtAddr};

pub const KERNEL_START: VirtAddr = VirtAddr::new(0x40000);
pub const KERNEL_STACK_START: VirtAddr = VirtAddr::new(0x100000);
pub const KERNEL_STACK_SIZE: u64 = 100 * 1024; // 100 KiB
#[expect(dead_code)]
pub const PROCESS_START: VirtAddr = VirtAddr::new(KERNEL_STACK_START.as_u64() + KERNEL_STACK_SIZE);
pub const PHYSICAL_MEMORY_OFFSET: VirtAddr = VirtAddr::new(0x20000000000);

pub const MAPPINGS: Mappings = {
    let mut mappings = Mappings::new_default();
    mappings.ramdisk_memory = Mapping::FixedAddress(KERNEL_START.as_u64());
    mappings.kernel_stack = Mapping::FixedAddress(KERNEL_STACK_START.as_u64());
    mappings.physical_memory = Some(Mapping::FixedAddress(PHYSICAL_MEMORY_OFFSET.as_u64()));
    mappings
};

/// Initialize a new OffsetPageTable.
pub unsafe fn init() -> OffsetPageTable<'static> {
    // SAFETY: called only once, and the bootloader mapped the virtual memory for us
    let level_4_table = unsafe { active_level_4_table() };
    // SAFETY: I trust active_level_4_table to give me a correct table
    unsafe { OffsetPageTable::new(level_4_table, PHYSICAL_MEMORY_OFFSET) }
}

/// Returns a mutable reference to the active level 4 table.
/// Should only be called once.
unsafe fn active_level_4_table() -> &'static mut PageTable {
    let (level_4_table_frame, _) = Cr3::read();

    let physical_address = level_4_table_frame.start_address();
    let page_table_ptr = phys_as_mut_ptr(physical_address.as_u64());

    // SAFETY: the function caller must guarantee that
    // the physical memory is mapped to virtual memory at the offset
    unsafe { &mut *page_table_ptr }
}

pub fn phys_as_ptr<T>(addr: u64) -> *const T {
    (PHYSICAL_MEMORY_OFFSET + addr).as_ptr()
}

pub fn phys_as_mut_ptr<T>(addr: u64) -> *mut T {
    (PHYSICAL_MEMORY_OFFSET + addr).as_mut_ptr()
}

/// A FrameAllocator that returns usable frames from the bootloader's memory map.
pub struct BootInfoFrameAllocator {
    memory_regions: &'static MemoryRegions,
    next: usize,
}

impl BootInfoFrameAllocator {
    /// Create a FrameAllocator from the passed memory map.
    ///
    /// This function is unsafe because the caller must guarantee that the passed
    /// memory map is valid. The main requirement is that all frames that are marked
    /// as `USABLE` in it are really unused.
    pub unsafe fn init(memory_regions: &'static MemoryRegions) -> Self {
        BootInfoFrameAllocator {
            memory_regions,
            next: 0,
        }
    }

    /// Returns an iterator over the usable frames specified in the memory map.
    fn usable_frames(&self) -> impl Iterator<Item = PhysFrame> {
        // get usable regions from memory map
        let regions = self.memory_regions.iter();
        let usable_regions = regions.filter(|r| r.kind == MemoryRegionKind::Usable);
        // map each region to its address range
        let addr_ranges = usable_regions.map(|r| r.start..r.end);
        // transform to an iterator of frame start addresses
        let frame_addresses = addr_ranges.flat_map(|r| r.step_by(4096));
        // create `PhysFrame` types from the start addresses
        frame_addresses.map(|addr| PhysFrame::containing_address(PhysAddr::new(addr)))
    }
}

// SAFETY: +1
unsafe impl FrameAllocator<Size4KiB> for BootInfoFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame> {
        let frame = self.usable_frames().nth(self.next);
        self.next += 1;
        frame
    }
}
