#![no_std]
#![no_main]
#![feature(abi_x86_interrupt)]
#![feature(asm_const)]
#![feature(generic_nonzero)]
#![feature(step_trait)]
#![feature(const_mut_refs)]
#![feature(lint_reasons)]
#![feature(strict_provenance)]
#![deny(unsafe_op_in_unsafe_fn)]
#![warn(clippy::undocumented_unsafe_blocks)]

extern crate alloc;

#[macro_use]
mod debug_console;
#[macro_use]
mod vga_buffer;

mod acpi;
mod allocator;
mod fixed_size_block;
mod interrupts;
mod lobstext2;
mod paging;
mod shell;
mod utils;

use bootloader_api::info::Optional;
use bootloader_api::{BootInfo, BootloaderConfig};
use core::panic::PanicInfo;
use core::{mem, slice};
use x86_64::instructions::hlt;

use crate::lobstext2::read_fs;
use crate::paging::{BootInfoFrameAllocator, PHYSICAL_MEMORY_OFFSET};
use crate::vga_buffer::{init_vga, Color};

const CONFIG: BootloaderConfig = {
    let mut config = BootloaderConfig::new_default();
    config.kernel_stack_size = paging::KERNEL_STACK_SIZE;
    config.mappings = paging::MAPPINGS;
    config
};
bootloader_api::entry_point!(kernel_main, config = &CONFIG);

fn kernel_main(boot_info: &'static mut BootInfo) -> ! {
    // -- Kernel initialization --
    if let Some(framebuffer) = boot_info.framebuffer.as_mut() {
        init_vga(framebuffer, Some(Color::white()));
    }

    let rsdp_addr = *boot_info.rsdp_addr.as_ref().expect("no RSDP detected");
    // SAFETY: we trust the bootloader to send us the correct RSDP, right?
    let sdt_iter = unsafe { acpi::sdt_iter(rsdp_addr) };

    let _facp = sdt_iter
        .clone()
        .find(|h| h.signature == *b"FACP")
        .expect("couldn't find FACP");
    let _hpet = sdt_iter
        .clone()
        .find(|h| h.signature == *b"HPET")
        .expect("couldn't find HPET");
    let madt = sdt_iter
        .clone()
        .find(|h| h.signature == *b"APIC")
        .expect("couldn't find MADT");
    // SAFETY: we know this is the MADT from the "APIC" signature
    let madt = unsafe { mem::transmute(madt) };

    // for sdt in sdt_iter {
    //     debug_println!("{}", core::str::from_utf8(&sdt.signature).unwrap());
    // }

    // SAFETY: called once :)
    unsafe {
        interrupts::init(madt);
    }

    assert_eq!(
        boot_info.physical_memory_offset,
        Optional::Some(PHYSICAL_MEMORY_OFFSET.as_u64())
    );

    // SAFETY: called only once
    let mut mapper = unsafe { paging::init() };
    // SAFETY: called only once
    let mut frame_allocator = unsafe { BootInfoFrameAllocator::init(&boot_info.memory_regions) };
    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("Heap initialisation failed.");

    debug_println!("Kernel initialized.\n");

    // -- After initialization --
    debug_println!("Hello, KrabbOS!");

    let ramdisk_addr = *boot_info.ramdisk_addr.as_mut().unwrap();
    let ramdisk_len = boot_info.ramdisk_len as usize;
    // SAFETY: BIOS guarantees ramdisk is valid for `ramdisk_len` bytes
    let ramdisk: &mut [u8] =
        unsafe { slice::from_raw_parts_mut(ramdisk_addr as *mut u8, ramdisk_len) };
    let lobstext2 = read_fs(ramdisk).unwrap();
    debug_println!("Kernel entered halting mode without crashing.");

    shell::start(lobstext2);
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    x86_64::instructions::interrupts::disable();
    debug_println!("PANIC: {}", info);
    loop {
        hlt();
    }
}
