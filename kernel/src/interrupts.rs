use core::{
    arch::{asm, x86_64::__cpuid},
    ptr,
};

use x86_64::{
    instructions::interrupts,
    structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode},
};

use crate::shell;
use crate::{
    acpi::{Madt, MadtRecord},
    paging::phys_as_mut_ptr,
    utils::OnceUnsafe,
    vga_buffer,
};

const INTERRUPT_VECTOR_APIC_TIMER: u8 = 0x20;
const INTERRUPT_VECTOR_KEYBOARD: u8 = 0x21;

const IA32_APIC_BASE_MSR: u32 = 0x1B;

#[expect(dead_code)]
const PIC1_COMMAND: u16 = 0x20;
const PIC1_DATA: u16 = 0x21;
#[expect(dead_code)]
const PIC2_COMMAND: u16 = 0xA0;
const PIC2_DATA: u16 = 0xA1;

const KEYBOARD_PORT: u16 = 0x60;

enum StandardIrq {
    Keyboard = 1,
}

// const APIC_BSP_FLAG: u64 = 0x100;
const APIC_GLOBAL_ENABLE_FLAG: u64 = 0x800;

static IDT: OnceUnsafe<InterruptDescriptorTable> = OnceUnsafe::new();

// FIXME: it doesn't make sense to store a *local* APIC in a global variable,
// but oh well
static LOCAL_APIC: OnceUnsafe<LocalApic> = OnceUnsafe::new();

/// Initialize interrupt handling
///
/// # Safety
///
/// This function should be called only once.
pub unsafe fn init(madt: &'static Madt) {
    debug_println!("Initializing interrupts");

    // sanity checks
    assert!(supports_apic(), "APIC not supported");
    assert!(supports_msr(), "MSR not supported");

    // SAFETY: the IDT is only read after it gets loaded, and the caller
    // guarantees no other thread is currently initializing it
    unsafe {
        init_idt();
    }

    disable_pic();

    let mut io_apic_addr = None;
    let mut local_apic_id = None;
    for record in madt.records() {
        match record {
            MadtRecord::ProcessorLocalAPIC(t) => local_apic_id = Some(t.apic_id),
            MadtRecord::InputOutputAPIC(t) => io_apic_addr = Some(t.addr),
            // FIXME: handle any interrupt source override
            // FIXME: handle any LAPIC address override as well
            _ => {}
        }
    }
    let local_apic_id = local_apic_id.expect("no local APIC in MADT");
    // SAFETY: an I/O APIC address fresh from the MADT
    let mut io_apic = unsafe {
        IoApic::new(phys_as_mut_ptr(
            io_apic_addr.expect("no I/O APIC detected") as u64
        ))
    };
    io_apic.set_redirection(
        StandardIrq::Keyboard as u8,
        INTERRUPT_VECTOR_KEYBOARD,
        local_apic_id as u32,
        false,
    );

    // Configure local APIC
    // SAFETY: Léo read the doc
    let apic_msr = unsafe { rdmsr(IA32_APIC_BASE_MSR) };
    // SAFETY: we just got the local APIC base pointer from the MSR
    let local_apic = unsafe { LocalApic::new(phys_as_mut_ptr(apic_msr & 0xFFFFFF000)) };
    let apic_global_enable = apic_msr & APIC_GLOBAL_ENABLE_FLAG != 0;
    if !apic_global_enable {
        // SAFETY: Léo read more of the doc
        unsafe {
            wrmsr(IA32_APIC_BASE_MSR, apic_msr | APIC_GLOBAL_ENABLE_FLAG);
        }
    }
    local_apic.setup_timer(
        INTERRUPT_VECTOR_APIC_TIMER,
        TimerMode::Periodic,
        TimerDivide::DivideBy32,
        0x10000,
    );
    local_apic.set_spurious_vector(0xFF); // as recommanded by the wiki
    local_apic.enable();
    // SAFETY: `LOCAL_APIC` is used to send EOI in the interrupt handlers, that
    // are only able to be called once interrupts are enabled, i.e. later
    unsafe {
        LOCAL_APIC.init(local_apic);
    }

    interrupts::enable();

    // In case the LAPIC has already recieved a keyboard interrupt, ack it
    // SAFETY: reading keyboard port
    let _ = unsafe { inb(KEYBOARD_PORT) };
    LOCAL_APIC.get().end_of_interrupt();
}

fn supports_apic() -> bool {
    // SAFETY: just CPUID
    unsafe { __cpuid(1) }.edx & 0x100 != 0
}

fn supports_msr() -> bool {
    // SAFETY: just CPUID
    unsafe { __cpuid(1) }.edx & 0x1 != 0
}

/// Initialize the Interrupt Descriptor Table (IDT)
///
/// # Safety
///
/// The caller must guarantee exclusive access to `IDT`
unsafe fn init_idt() {
    macro_rules! register_fault_handler {
        ($idt:ident => {$($exc:ident),* $(,)?}) => {
            $(
                extern "x86-interrupt" fn $exc(stack_frame: InterruptStackFrame) {
                    panic!(concat!("EXCEPTION ", stringify!($exc), "\n{:#?}"), stack_frame);
                }
                $idt.$exc.set_handler_fn($exc);
            )*
        };
    }

    let mut idt = InterruptDescriptorTable::new();
    idt.page_fault.set_handler_fn(page_fault_handler);
    idt.double_fault.set_handler_fn(double_fault_handler);
    register_fault_handler!(idt => {
        bound_range_exceeded,
        device_not_available,
        divide_error,
        invalid_opcode,
        overflow,
    });
    idt[INTERRUPT_VECTOR_APIC_TIMER].set_handler_fn(timer_handler);
    idt[INTERRUPT_VECTOR_KEYBOARD].set_handler_fn(keyboard_handler);
    // SAFETY: caller guarantees writing to IDT is fiiiine
    unsafe {
        IDT.init(idt);
    }
    IDT.get().load()
}

// FIXME: the OSDev wiki recommands to redirect the interrupt vectors in
// case a spurious IRQ happens, but under QEMU I haven't seen anything yet
fn disable_pic() {
    // SAFETY: writing to PIC data ports
    unsafe {
        outb(PIC1_DATA, 0xFF);
        outb(PIC2_DATA, 0xFF);
    }
}

#[allow(dead_code)]
enum TimerDivide {
    DivideBy1 = 0b1011,
    DivideBy2 = 0b0000,
    DivideBy4 = 0b0001,
    DivideBy8 = 0b0010,
    DivideBy16 = 0b0011,
    DivideBy32 = 0b1000,
    DivideBy64 = 0b1001,
    DivideBy128 = 0b1010,
}

/// The different [modes] the APIC timer can run in
///
/// [modes]: https://wiki.osdev.org/APIC_Timer#APIC_Timer_Modes
#[allow(dead_code)]
enum TimerMode {
    OneShot = 0b00,
    Periodic = 0b01,
    TscDeadline = 0b10,
}

struct LocalApic {
    base: *mut u32,
}

impl LocalApic {
    const REG_EOI: isize = 0xB0;
    const REG_SPURIOUS_INTERRUPT_VECTOR: isize = 0xF0;
    const REG_LVT_TIMER: isize = 0x320;
    const REG_TIMER_INITIAL_COUNT: isize = 0x380;
    const REG_TIMER_CURRENT_COUNT: isize = 0x390;
    const REG_TIMER_DIVIDE_CONFIG: isize = 0x3E0;

    /// # Safety
    ///
    /// The caller must ensure `base` is the local APIC's base pointer
    unsafe fn new(base: *mut u32) -> LocalApic {
        LocalApic { base }
    }

    fn enable(&self) {
        // SAFETY: accessing memory-mapped Local APIC registers
        unsafe {
            let svr =
                ptr::read_volatile(self.base.byte_offset(Self::REG_SPURIOUS_INTERRUPT_VECTOR));
            ptr::write_volatile(
                self.base.byte_offset(Self::REG_SPURIOUS_INTERRUPT_VECTOR),
                svr | 0x100,
            );
        }
    }

    fn set_spurious_vector(&self, vector: u8) {
        // SAFETY: accessing memory-mapped Local APIC registers
        unsafe {
            let svr =
                ptr::read_volatile(self.base.byte_offset(Self::REG_SPURIOUS_INTERRUPT_VECTOR))
                    & 0xFFFFFFF0;
            ptr::write_volatile(
                self.base.byte_offset(Self::REG_SPURIOUS_INTERRUPT_VECTOR),
                svr | vector as u32,
            );
        }
    }

    /// Setup the local APIC timer
    fn setup_timer(&self, vector: u8, mode: TimerMode, divide: TimerDivide, count: u32) {
        // SAFETY: accessing memory-mapped Local APIC registers
        unsafe {
            let dcr = ptr::read_volatile(self.base.byte_offset(Self::REG_TIMER_DIVIDE_CONFIG))
                & 0xFFFFFFF0;
            ptr::write_volatile(
                self.base.byte_offset(Self::REG_TIMER_DIVIDE_CONFIG),
                dcr | divide as u32,
            );

            let lvtt = ptr::read_volatile(self.base.byte_offset(Self::REG_LVT_TIMER)) & 0xFFFEFF00;
            ptr::write_volatile(
                self.base.byte_offset(Self::REG_LVT_TIMER),
                lvtt | vector as u32 | ((mode as u32) << 17),
            );

            ptr::write_volatile(self.base.byte_offset(Self::REG_TIMER_INITIAL_COUNT), count);
            ptr::write_volatile(self.base.byte_offset(Self::REG_TIMER_CURRENT_COUNT), count);
        }
    }

    fn end_of_interrupt(&self) {
        // SAFETY: accessing memory-mapped Local APIC registers
        unsafe {
            ptr::write_volatile(self.base.byte_offset(Self::REG_EOI), 0);
        }
    }
}

// Those are fine but don't make any sense for a *local* APIC
// SAFETY: don't worry be happy
unsafe impl Send for LocalApic {}
// SAFETY: don't worry be happy
unsafe impl Sync for LocalApic {}

struct IoApic {
    base: *mut u32,
}

impl IoApic {
    /// # Safety
    ///
    /// The caller must ensure `base` is an I/O APIC's base pointer
    unsafe fn new(base: *mut u32) -> IoApic {
        IoApic { base }
    }

    fn read_reg(&self, reg: u8) -> u32 {
        assert!(reg < 0x40);
        // SAFETY: we just checked `reg` is in bounds of the APIC registers
        unsafe {
            ptr::write_volatile(self.base, reg as u32);
            ptr::read_volatile(self.base.offset(4))
        }
    }

    unsafe fn write_reg(&mut self, reg: u8, value: u32) {
        assert!(reg < 0x40);
        // SAFETY: we just checked `reg` is in bounds of the APIC registers.
        // Whether the written value makes sense is up to the caller.
        unsafe {
            ptr::write_volatile(self.base, reg as u32);
            ptr::write_volatile(self.base.offset(4), value);
        }
    }

    // TODO: maybe set all the other fields of the redirection register to
    // something else than 0
    fn set_redirection(&mut self, irq: u8, vector: u8, destination: u32, masked: bool) {
        assert!(irq < 24);
        let reg = 0x10 + 2 * irq;
        // SAFETY: one day I'll write a proper bit-packed structure. One day.
        unsafe {
            let redirection_entry_low = self.read_reg(reg) & 0xFFFE5000; // keep reserved bits
            self.write_reg(
                reg,
                redirection_entry_low | vector as u32 | ((masked as u32) << 16),
            );
            let redirection_entry_high = self.read_reg(reg + 1) & 0x0007FFFF;
            self.write_reg(reg + 1, redirection_entry_high | (destination << 24));
        }
    }

    #[expect(dead_code)]
    fn set_masked(&mut self, irq: u8, masked: bool) {
        assert!(irq < 24);
        let reg = 0x10 + 2 * irq;
        // SAFETY: mask bit is bit 16, is all you need to know
        unsafe {
            let reg_lo = self.read_reg(reg) & !(1 << 16);
            self.write_reg(reg, reg_lo | ((masked as u32) << 16));
        }
    }
}

extern "x86-interrupt" fn page_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    panic!("PAGE FAULT {:?}:\n{:#?}", error_code, stack_frame);
}

extern "x86-interrupt" fn double_fault_handler(
    stack_frame: InterruptStackFrame,
    _error_code: u64,
) -> ! {
    panic!("DOUBLE FAULT:\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn timer_handler(_stack_frame: InterruptStackFrame) {
    // print!(".");
    LOCAL_APIC.get().end_of_interrupt();
}

extern "x86-interrupt" fn keyboard_handler(_stack_frame: InterruptStackFrame) {
    use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};
    use spin::Mutex;

    static KEYBOARD: Mutex<Keyboard<layouts::Azerty, ScancodeSet1>> = Mutex::new(Keyboard::new(
        ScancodeSet1::new(),
        layouts::Azerty,
        HandleControl::Ignore,
    ));

    let mut keyboard = KEYBOARD.lock();

    // SAFETY: reading the keyboard
    let scancode = unsafe { inb(KEYBOARD_PORT) };

    if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
        if key_event.code == pc_keyboard::KeyCode::Backspace
            && key_event.state != pc_keyboard::KeyState::Up
        {
            if shell::backspace() {
                vga_buffer::backspace();
            }
        } else if key_event.code == pc_keyboard::KeyCode::Delete
            && key_event.state != pc_keyboard::KeyState::Up
        {
            if shell::delete() {
                vga_buffer::delete();
            }
        } else if let Some(key) = keyboard.process_keyevent(key_event) {
            match key {
                DecodedKey::Unicode(character) => shell::handle_char(character),
                // DecodedKey::RawKey(pc_keyboard::KeyCode::Backspace) => function_that_gets_called_when_we_input_backspace_and_that_removes_the_last_char_on_the_column(),
                DecodedKey::RawKey(key) => shell::handle_raw_key(key), //println!("\nEVENEMENT: {:?}", key),
            }
        }
    }
    LOCAL_APIC.get().end_of_interrupt();
}

unsafe fn inb(port: u16) -> u8 {
    let data;
    // SAFETY: up to the caller
    unsafe {
        asm!("in al, dx", in("dx") port, out("al") data);
    }
    data
}

unsafe fn outb(port: u16, data: u8) {
    // SAFETY: up to the caller
    unsafe {
        asm!("out dx, al", in("dx") port, in("al") data);
    }
}

unsafe fn rdmsr(msr: u32) -> u64 {
    let (mut high, mut low): (u32, u32);
    // SAFETY: up to the caller
    unsafe {
        asm!("rdmsr", in("ecx") msr, out("eax") low, out("edx") high);
    }
    ((high as u64) << 32) | low as u64
}

unsafe fn wrmsr(msr: u32, data: u64) {
    let low = (data & 0xffffffff) as u32;
    let high = (data >> 32) as u32;
    // SAFETY: up to the caller
    unsafe {
        asm!("wrmsr", in("ecx") msr, in("eax") low, in("edx") high);
    }
}
