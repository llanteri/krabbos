use core::cell::OnceCell;

use crate::alloc::string::String;
use crate::alloc::vec::Vec;
use crate::lobstext2::{self, Ext2, Size512, Synced};
use crate::vga_buffer;
use crate::vga_buffer::{set_color, set_cursor, set_weight, Color};
use alloc::borrow::ToOwned;
use alloc::format;
use alloc::string::ToString;
use noto_sans_mono_bitmap::FontWeight;
use pc_keyboard::KeyCode;
use regex::Regex;
use x86_64::instructions::hlt;

static mut LINE_BUFFER: String = String::new();
static mut CURSOR: usize = 0;
static mut PREVIOUS_COMMANDS: Vec<String> = Vec::new();

#[derive(PartialEq, Eq)]
enum Mode {
    Shell,
    Slides,
    Image,
    Less,
}

static mut SLIDES_MODE: Mode = Mode::Shell;
static mut SLIDES_GOTO: Option<usize> = None;
static mut CURRENT_SLIDE: usize = 0;
static mut CURRENT_FILE: String = String::new();
static mut MAX_SLIDE: usize = 0;

static mut FILE_SYSTEM: OnceCell<Synced<Ext2<Size512, &'static mut [u8]>>> = OnceCell::new();
static mut CWD: Vec<u8> = Vec::new();

static MEMES: &'static [&[u8]] = &[
    include_bytes!("../../memes/0.bmp"),
    include_bytes!("../../memes/1.bmp"),
    include_bytes!("../../memes/2.bmp"),
    include_bytes!("../../memes/3.bmp"),
    include_bytes!("../../memes/4.bmp"),
    include_bytes!("../../memes/5.bmp"),
    include_bytes!("../../memes/6.bmp"),
    include_bytes!("../../memes/7.bmp"),
    include_bytes!("../../memes/8.bmp"),
    include_bytes!("../../memes/9.bmp"),
    include_bytes!("../../memes/10.bmp"),
    include_bytes!("../../memes/11.bmp"),
    include_bytes!("../../memes/12.bmp"),
    include_bytes!("../../memes/13.bmp"),
    include_bytes!("../../memes/krabboss.bmp"),
];

pub fn print_crab() {
    println!(include_str!("logo.txt"));
}

/// Initializes the shell.
pub fn start(filesystem: Synced<Ext2<Size512, &'static mut [u8]>>) -> ! {
    unsafe {
        CWD = b"/".to_vec();
        FILE_SYSTEM.set(filesystem).unwrap();
    }

    set_weight(FontWeight::Bold);
    set_color(Color::orange());
    print_crab();
    set_cursor(true);
    set_color(Color::white());
    set_weight(FontWeight::Regular);

    for _ in 0..500 {
        hlt();
    }

    prompt();

    halt();
}

/// When in slide mode, prints the slide.
fn display_current_slide() {
    if unsafe { SLIDES_MODE == Mode::Slides } {
        let line = unsafe { CURRENT_SLIDE };
        let path_string = pathify(&format!("{line}.txt"));
        let slide_path = path_string.as_bytes();
        match lobstext2::cat(slide_path, unsafe { &FILE_SYSTEM.get().unwrap() }) {
            Ok(slides_text) => {
                if unsafe { CURRENT_SLIDE == 0 } {
                    vga_buffer::clear();
                }

                println!("{}", slides_text);
                println!(
    "  <  q  >                                                                                             Slide  {}/{} ",
            unsafe { CURRENT_SLIDE+1 },
            unsafe { MAX_SLIDE }
        );
            }
            Err(_) => {
                vga_buffer::set_weight(FontWeight::Regular);
                println!("slides: file {} does not exist", unsafe { CURRENT_SLIDE });
                exit_slides_mode();
            }
        }
    } else if unsafe { SLIDES_MODE == Mode::Less } {
        let line = unsafe { CURRENT_SLIDE };
        match lobstext2::cat(unsafe { CURRENT_FILE.as_bytes() }, unsafe {
            &FILE_SYSTEM.get().unwrap()
        }) {
            Ok(file_text) => {
                if unsafe { CURRENT_SLIDE == 0 } {
                    vga_buffer::clear();
                }

                let to_show = file_text.split("\n").skip(line * 31).take(31);
                println!(
                    "{}",
                    to_show
                        .clone()
                        .map(|s| s.to_string())
                        .collect::<Vec<String>>()
                        .join("\n")
                );
                for _ in 0..(31-to_show.count()) {
                    println!("");
                }
                vga_buffer::set_color(Color::orange());
                println!(
    "  <  q  >                                                                                             Page  {}/{} ",
            unsafe { CURRENT_SLIDE+1 },
            unsafe { MAX_SLIDE }
        );
                vga_buffer::set_color(Color::white());
            }
            Err(_) => {
                vga_buffer::set_weight(FontWeight::Regular);
                println!("slides: file {} does not exist", unsafe { CURRENT_SLIDE });
                exit_slides_mode();
            }
        }
    }
}

/// Disables slides mode and reset shell prompt.
fn exit_slides_mode() {
    unsafe {
        SLIDES_MODE = Mode::Shell;
        CURRENT_SLIDE = 0;
    }
    vga_buffer::set_weight(FontWeight::Regular);
    vga_buffer::clear();
    vga_buffer::set_cursor(true);
    prompt();
}

/// Called by the interrupts handler when a char key is pressed.
/// Prints the key or disables slides mode.
pub fn handle_char(character: char) {
    unsafe {
        if SLIDES_MODE == Mode::Slides || SLIDES_MODE == Mode::Image || SLIDES_MODE == Mode::Less {
            if character == 'q' {
                exit_slides_mode();
                return;
            }
        }
        if SLIDES_MODE == Mode::Slides {
            if let Some(slide) = SLIDES_GOTO {
                if let Some(i) = char::to_digit(character, 10) {
                    SLIDES_GOTO = Some(slide * 10 + i as usize);
                } else if character == '\n' {
                    SLIDES_GOTO = None;
                    if slide > 0 && slide <= MAX_SLIDE {
                        CURRENT_SLIDE = slide - 1;
                        display_current_slide();
                    }
                } else {
                    SLIDES_GOTO = None;
                }
            }
            if character == 'g' {
                SLIDES_GOTO = Some(0);
            }
            return;
        }
        print!("{}", character);
        if character == '\n' {
            let string = LINE_BUFFER.clone();
            CURSOR = 0;
            LINE_BUFFER.clear();

            if exec_command(string) {
                prompt();
            }
        } else {
            LINE_BUFFER.insert(CURSOR, character);
            CURSOR += 1;
        }
    }
}

/// Called by the interrupts handler when a raw key is pressed.
/// Handles cursor movement (left or right) and history.
pub fn handle_raw_key(key: KeyCode) {
    match key {
        pc_keyboard::KeyCode::ArrowLeft => {
            if go_left() {
                vga_buffer::we_go_left();
            }
        }
        pc_keyboard::KeyCode::ArrowRight => {
            if go_right() {
                vga_buffer::we_go_right();
            }
        }
        pc_keyboard::KeyCode::ArrowUp => {
            //vga_buffer::we_go_up();
            if unsafe { !PREVIOUS_COMMANDS.is_empty() } {
                print!("{}", unsafe {
                    PREVIOUS_COMMANDS[PREVIOUS_COMMANDS.len() - 1].clone()
                });
                unsafe {
                    LINE_BUFFER = PREVIOUS_COMMANDS[PREVIOUS_COMMANDS.len() - 1]
                        .clone()
                        .chars()
                        .into_iter()
                        .collect();
                }
            }
        }
        pc_keyboard::KeyCode::ArrowDown => (), //vga_buffer::we_go_down(),
        _ => (),
    }
    //debug_println!("\nEVENEMENT: {:?}", key);
}

/// Called by the interrupts handler when backspace is pressed.
/// Removes a char from the buffer and moves the cursor.
pub fn backspace() -> bool {
    unsafe {
        if SLIDES_MODE != Mode::Shell {
            return false;
        }
        if CURSOR > 0 {
            CURSOR -= 1;
            LINE_BUFFER.remove(CURSOR);
            return true;
        } else {
            return false;
        }
    }
}

pub fn delete() -> bool {
    false
}

/// Return true when the VGA buffer should be updated.
pub fn go_left() -> bool {
    if unsafe { SLIDES_MODE == Mode::Slides || SLIDES_MODE == Mode::Less } {
        if unsafe { CURRENT_SLIDE } > 0 {
            unsafe {
                CURRENT_SLIDE -= 1;
            }
            display_current_slide();
        }
        return false;
    }

    if unsafe { CURSOR > 0 } {
        unsafe {
            CURSOR -= 1;
        }
        return true;
    } else {
        return false;
    }
}
pub fn go_right() -> bool {
    if unsafe { SLIDES_MODE == Mode::Slides || SLIDES_MODE == Mode::Less } {
        if unsafe { CURRENT_SLIDE < MAX_SLIDE } {
            unsafe {
                CURRENT_SLIDE += 1;
            }
            display_current_slide();
        }
        return false;
    }

    if unsafe { CURSOR < LINE_BUFFER.len() } {
        unsafe {
            CURSOR += 1;
        }
        return true;
    } else {
        return false;
    }
}

/// Prints the shell prompt.
fn prompt() {
    set_weight(FontWeight::Bold);
    set_color(Color::orange());
    print!("krabbos");
    set_color(Color::white());
    print!(":");
    set_color(Color::light_blue());
    print!(
        "{}",
        String::from_utf8_lossy(unsafe { CWD.clone() }.as_slice())
    );
    set_color(Color::orange());
    print!(" > ");
    set_color(Color::white());
    set_weight(FontWeight::Regular);
}

/// Transforms a path given by the user (absolute or relative) into absolute path.
fn pathify(path: &String) -> String {
    let string_path = match path.chars().next() {
        Some('/') => {
            // absolute path
            path.to_owned()
        }
        Some(_) => {
            // relative path
            let mut cwd = String::from_utf8_lossy(unsafe { CWD.as_slice() }).to_string();
            if cwd == "/" {
                cwd = "".to_string();
            }
            let s = format!("{}/{}", cwd.as_str().to_string(), path);
            s
        }
        // empty
        None => unsafe { String::from_utf8_lossy(CWD.as_slice()).to_string() },
    };

    let mut heap = Vec::new();
    for segment in string_path.split("/") {
        if segment == ".." {
            // double dots go backwards
            if heap.len() >= 1 {
                heap.pop();
            }
        } else if segment != "." && segment != "" {
            // ignore dots
            heap.push(segment.to_string());
        }
    }

    let mut s = heap.join("/");
    s.insert(0, '/');
    s
}

/// Interprets the given string as a command.
/// Returns true when the prompt should be send again.
fn exec_command(command: String) -> bool {
    unsafe {
        PREVIOUS_COMMANDS.push(command.clone());
    }

    // regex for different commands
    let echo_re = Regex::new(r"( )*echo( )*(?<text>[0-9A-Za-z_ ]*)").unwrap();
    let ls_re = Regex::new(r"( )*ls( )*(?<path>[0-9A-Za-z_/]*)").unwrap();
    let cat_re = Regex::new(r"( )*cat( )*(?<filename>[/0-9A-Za-z_\.]*)( )*").unwrap();
    let less_re = Regex::new(r"( )*less( )*(?<filename>[/0-9A-Za-z_\.]*)( )*").unwrap();
    let cd_re = Regex::new(r"( )*cd (?<dirname>[/0-9A-Za-z_\.]+)( )*").unwrap();
    let slides_re = Regex::new(r"( )*slides( )*").unwrap(); //Regex::new(r"( )*slides (?<filename>[0-9A-Za-z_\.]+)( )*").unwrap();
    let clear_re = Regex::new(r"( )*clear( )*").unwrap();
    let pwd_re = Regex::new(r"( )*pwd( )*").unwrap();
    let imshow_re = Regex::new(r"( )*imshow( )*(?<filename>[0-9]+)\.bmp( )*").unwrap();
    //let imshow_re = Regex::new(r"( )*imshow( )*(?<filename>[/0-9A-Za-z_\.]*)( )*").unwrap();
    let krab_re = Regex::new(r"( )*krab( )*").unwrap();

    // echo
    if let Some(captures) = echo_re.captures(&command) {
        println!("{}", captures["text"].to_string());
    }
    // clear
    else if clear_re.is_match(&command) {
        vga_buffer::clear();
    }
    // pwd
    else if pwd_re.is_match(&command) {
        println!("{}", String::from_utf8_lossy(unsafe { CWD.as_slice() }));
    }
    // ls
    else if let Some(captures) = ls_re.captures(&command) {
        let dirname = captures["path"].to_string();
        let path_string = pathify(&dirname);
        let path = path_string.as_bytes();

        match crate::lobstext2::ls(path, unsafe { &FILE_SYSTEM.get().unwrap() }) {
            Ok(file_list) => {
                for (filename, is_directory) in file_list {
                    if is_directory {
                        set_color(Color::light_blue());
                    }
                    println!("{}", filename);
                    if is_directory {
                        set_color(Color::white());
                    }
                }
            }
            Err(e) => println!("ls: {:?}", e),
        }
    }
    // cat
    else if let Some(captures) = cat_re.captures(&command) {
        let dirname = captures["filename"].to_string();
        let path_string = pathify(&dirname);
        let path = path_string.as_bytes();

        let res = lobstext2::cat(path, unsafe { &FILE_SYSTEM.get().unwrap() });

        vga_buffer::set_cursor(false);
        match res {
            Ok(content) => println!("{}", content),
            Err(e) => println!("cat: {:?}", e),
        }
        vga_buffer::set_cursor(true);
    }
    // less
    else if let Some(captures) = less_re.captures(&command) {
        let dirname = captures["filename"].to_string();
        let path_string = pathify(&dirname);
        let path = path_string.as_bytes();

        let res = lobstext2::cat(path, unsafe { &FILE_SYSTEM.get().unwrap() });

        vga_buffer::set_cursor(false);
        unsafe {
            SLIDES_MODE = Mode::Less;
            CURRENT_FILE = path_string;
        }
        match res {
            Ok(content) => {
                unsafe { MAX_SLIDE = content.split("\n").count() / 31 + 1 }
                display_current_slide();
                return false;
            }
            Err(e) => println!("less: {:?}", e),
        }
    }
    // imshow
    else if let Some(captures) = imshow_re.captures(&command) {
        let dirname = captures["filename"].to_string();
        let path_string = pathify(&dirname);
        let _path = path_string.as_bytes();

        vga_buffer::set_cursor(false);
        unsafe {
            SLIDES_MODE = Mode::Image;
        }
        vga_buffer::clear();
        //vga_buffer::display_bmp(include_bytes!("../../krabboss.bmp"));
        vga_buffer::display_bmp(MEMES[dirname.parse::<usize>().unwrap()]);
        return false;
    }
    // cd
    else if let Some(captures) = cd_re.captures(&command) {
        let dirname = captures["dirname"].to_string();
        let path_string = pathify(&dirname);
        let new_path = path_string.as_bytes();

        match unsafe { FILE_SYSTEM.get() }.unwrap().find_inode(new_path) {
            Ok(inode) => {
                if inode.0.is_dir() {
                    unsafe { CWD = new_path.to_vec() }
                } else {
                    println!("cd: not a directory: {}", dirname)
                }
            }
            Err(e) => println!("cd: {:?}", e),
        }
    }
    // slides
    else if slides_re.is_match(&command) {
        unsafe {
            SLIDES_MODE = Mode::Slides;
            CURRENT_SLIDE = 0;
            MAX_SLIDE = crate::lobstext2::ls(&CWD.as_slice(), &FILE_SYSTEM.get().unwrap())
                .unwrap()
                .len();
        }
        vga_buffer::set_cursor(false);
        vga_buffer::set_weight(FontWeight::Bold);
        display_current_slide();
        return false;
    }
    // krab
    else if krab_re.is_match(&command) {
        set_weight(FontWeight::Bold);
        set_color(Color::orange());
        vga_buffer::clear();
        set_cursor(false);
        print_crab();
        set_cursor(true);
        set_color(Color::white());
        set_weight(FontWeight::Regular);
    }
    // unrecognized
    else {
        println!(
            "KrabbOSshell: command not found: {}",
            command.split_ascii_whitespace().next().unwrap_or("")
        );
    }
    true
}

fn halt() -> ! {
    loop {
        hlt();
    }
}
