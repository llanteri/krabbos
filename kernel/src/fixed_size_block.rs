const BLOCK_SIZES: &[usize] = &[8, 16, 32, 64, 128, 256, 512, 1024, 2048];

use alloc::alloc::GlobalAlloc;
use alloc::alloc::Layout;
use core::ptr;
use core::{mem, ptr::NonNull};

/// A wrapper around spin::Mutex to permit trait implementations.
pub struct Locked<A> {
    inner: spin::Mutex<A>,
}

impl<A> Locked<A> {
    pub const fn new(inner: A) -> Self {
        Locked {
            inner: spin::Mutex::new(inner),
        }
    }

    pub fn lock(&self) -> spin::MutexGuard<A> {
        self.inner.lock()
    }
}

struct ListNode {
    next: Option<&'static mut ListNode>,
}

pub struct FixedSizeBlockAllocator {
    list_heads: [Option<&'static mut ListNode>; BLOCK_SIZES.len()],
    fallback_allocator: linked_list_allocator::Heap,
}

impl FixedSizeBlockAllocator {
    pub const fn new() -> Self {
        const EMPTY: Option<&'static mut ListNode> = None;
        FixedSizeBlockAllocator {
            list_heads: [EMPTY; BLOCK_SIZES.len()],
            fallback_allocator: linked_list_allocator::Heap::empty(),
        }
    }

    /// Initialize the allocator with the given heap bounds. Must be called only once.
    /// The heap bounds must be valid and the heap must be unused
    pub unsafe fn init(&mut self, heap_start: usize, heap_size: usize) {
        // Check that all block sizes are powers of 2
        for size in BLOCK_SIZES {
            debug_assert_eq!(size & (size - 1), 0);
        }
        // SAFETY: caller guarantees
        unsafe {
            self.fallback_allocator.init(heap_start, heap_size);
        }
    }

    /// Allocates using the fallback allocator.
    fn fallback_alloc(&mut self, layout: Layout) -> *mut u8 {
        match self.fallback_allocator.allocate_first_fit(layout) {
            Ok(ptr) => ptr.as_ptr(),
            Err(_) => ptr::null_mut(),
        }
    }
}

/// Return the index of the chosen block size for the given layout.
fn list_index(layout: &Layout) -> Option<usize> {
    let at_least = layout.size().max(layout.align());
    BLOCK_SIZES.iter().position(|&s| s >= at_least)
}
// SAFETY: see below
unsafe impl GlobalAlloc for Locked<FixedSizeBlockAllocator> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let mut allocator = self.lock();
        match list_index(&layout) {
            Some(index) => {
                match allocator.list_heads[index].take() {
                    Some(node) => {
                        allocator.list_heads[index] = node.next.take();
                        node as *mut ListNode as *mut u8
                    }
                    None => {
                        // If no block exists, add a new block
                        let block_size = BLOCK_SIZES[index];
                        // only works if all block sizes are a power of 2
                        let block_align = block_size;
                        let layout = Layout::from_size_align(block_size, block_align).unwrap();
                        allocator.fallback_alloc(layout)
                    }
                }
            }
            None => allocator.fallback_alloc(layout),
        }
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        let mut allocator = self.lock();
        match list_index(&layout) {
            Some(index) => {
                let new_node = ListNode {
                    next: allocator.list_heads[index].take(),
                };
                // verify that block has size and alignment required for storing node
                assert!(
                    mem::size_of::<ListNode>() <= BLOCK_SIZES[index],
                    "Block size is two small to store node..."
                );
                assert!(
                    mem::align_of::<ListNode>() <= BLOCK_SIZES[index],
                    "Block is not aligned to store node..."
                );
                let new_node_ptr = ptr as *mut ListNode;
                // SAFETY: we juste created the block so it's free!
                unsafe { new_node_ptr.write(new_node) };
                // SAFETY: same
                allocator.list_heads[index] = Some(unsafe { &mut *new_node_ptr });
            }
            None => {
                let ptr = NonNull::new(ptr).unwrap();
                // SAFETY: we assume that the fallback allocator is safe
                unsafe { allocator.fallback_allocator.deallocate(ptr, layout) };
            }
        }
    }
}
